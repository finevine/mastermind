# Generated by Django 3.2 on 2021-05-03 09:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_alter_user_username'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='username',
            field=models.CharField(max_length=70, unique=True, verbose_name='Pseudo'),
        ),
    ]
