import os

from django.contrib.auth import get_user_model
from django.db import models
from django.dispatch import receiver

User = get_user_model()


@receiver(models.signals.pre_save, sender=User)
def auto_delete_avatar_on_user_change(sender, instance, **kwargs):  # pragma: no cover
    """
    Deletes old file from filesystem
    when corresponding `User` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = User.objects.get(pk=instance.pk).avatar
    except User.DoesNotExist:
        return False

    new_file = instance.avatar
    if old_file and old_file != new_file and os.path.isfile(old_file.path):
        os.remove(old_file.path)
