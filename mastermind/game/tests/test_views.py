import json
from mastermind.game.models import Game

from django.contrib import auth
from django.urls import reverse
from django.core.exceptions import PermissionDenied

from tests.custom_test_case import CustomTestCase

User = auth.get_user_model()


class TestGame(CustomTestCase):

    def test_create_game(self):
        """ Test new game """
        self.connectplayer1()
        game_num = len(self.player1.games.all())
        response = self.client.get(
            reverse('game:new_game', kwargs={'go': 'go'}),
            follow=True)
        self.assertEqual(len(self.player1.games.all()), game_num + 1)
        self.assertTemplateUsed(response, 'game/go.html')


    def test_create_giveup(self):
        """ Test giveupgame game """
        self.connectplayer1()
        self.creategame()
        self.assertIsNone(self.game1.date_end)
        response = self.client.get(
            reverse('game:giveup', kwargs={'game_id': self.game1.pk})
            )
        self.game1.refresh_from_db()
        self.assertIsNotNone(self.game1.date_end)
        self.assertEqual(self.game1.win, False)        
        

    def test_create_guess(self):
        """ Test new game """
        self.connectplayer1()
        self.creategame()
        data = {'guess': '0000'}
        response = self.client.post(
            reverse('game:guess', kwargs={'game_id': self.game2.pk}),
            data=data
            )
        self.assertJSONEqual(
            response.content,
            {'good_place': 0, 'wrong_place': 0, 'gameover': False}
            )
        

    def test_delete_game_wrong_user(self):
        """ Test delete a game """
        self.connectplayer2()
        self.creategame()
        with self.assertRaises(PermissionDenied):
            self.client.post(
                reverse('game:delete', kwargs={'game_id': self.game1.pk})
            )

    def test_delete_game_wrong_user(self):
        """ Test delete a game """
        self.connectplayer1()
        self.creategame()
        self.assertEqual(Game.objects.filter(player=self.player1).count(), 2)
        self.client.post(
            reverse('game:delete', kwargs={'game_id': self.game1.pk})
        )
        self.assertEqual(Game.objects.filter(player=self.player1).count(), 1)
        