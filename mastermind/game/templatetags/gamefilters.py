from django import template

register = template.Library()

@register.filter()
def range(value, max=None): # pragma: no cover
    if not max:
        return range(1, value)
    return range(value, max)


@register.filter()
def color(value, num):
    try:
        return value[num]
    except IndexError:
        return ''

@register.filter
def duration(duration):  # pragma: no cover
    if duration:
        total_seconds = int(duration.total_seconds())
        hours, remainder = divmod(total_seconds,60*60)
        minutes, seconds = divmod(remainder,60)
        total_seconds = int(duration.total_seconds())
        f = lambda x : '0'+str(x) if x <= 9 else str(x)

        return f'{f(hours)}:{f(minutes)}:{f(seconds)}'
    return ''
