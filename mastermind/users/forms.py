import datetime
from crispy_forms.bootstrap import AppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field, Layout
from django import forms
from django.contrib.auth import forms as auth_forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models
from django.utils import timezone
from django.utils.functional import lazy
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from mastermind.users.utils import resize_square
from PIL import Image

User = get_user_model()
mark_safe_lazy = lazy(mark_safe, str)


class LoginForm(auth_forms.AuthenticationForm):
# class LoginForm(forms.Form):
    """ Simple form for login """
    email = forms.EmailField(
        label="Email",
        widget=forms.TextInput(attrs={'placeholder': _('Email*')}))
    password = forms.CharField(label=_("Password*"),
        widget=forms.PasswordInput(attrs={'placeholder': _('Password*')}))
    stay_connected = forms.BooleanField(label=_("Remember me"), required=False)

    def __init__(self, *args, **kwargs):
        """ Init styles necessary for crispy form """
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.fields['email'].label = False
        self.fields['password'].label = False



class UserChangeForm(auth_forms.UserChangeForm):
    """ form for user change in admin """
    class Meta(auth_forms.UserChangeForm.Meta):
        model = User
        fields = ['email', 'password', 'language']


#####################################################
#                   USERCREATION                    #
#####################################################
class UserCreationForm(auth_forms.UserCreationForm):
    """ Form for signin without username and with CGU acceptation """
    # username = None
    email = models.EmailField(
        unique=True, max_length=255, blank=False,)
    password1 = forms.CharField(label=_("Password*"),
        widget=forms.PasswordInput(attrs={'placeholder': _('Password*')}))
    accept_terms = mark_safe_lazy(
        _('I have red and accept the <a href="/terms">Terms</a> to subscribe.'))
    cgu_check = forms.BooleanField(
        label=_("Terms"),
        required=True,
        help_text=accept_terms
    )

    def __init__(self, *args, **kwargs):
        """ Init styles necessary for crispy form """
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.fields['username'].widget.attrs['placeholder'] = _('Nickname*')
        self.fields['username'].label = False
        self.fields['email'].widget.attrs['placeholder'] = _('Email*')
        self.fields['email'].label = False
        self.fields['password1'].label = False
        self.fields['password2'].label = False
        self.fields['password2'].widget.attrs['placeholder'] = _('Confirm password*')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'cgu_check')
        help_texts = {'password1': None,}

    def clean_email(self):
        """ Check if already used email """
        email = self.cleaned_data["email"]
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email
        self.add_error('email', _("This email address is already used"))
        return email


#####################################################
#                   USERUPDATE                      #
#####################################################
class UserUpdateForm(forms.ModelForm):
    """ Let user update his own informations """
    error_messages = auth_forms.UserCreationForm.error_messages.update(
        {"duplicate_email": _("This email address is already used")}
    )
    delete_avatar = forms.BooleanField(label=_("Delete my profile picture"), required=False)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'language',
            'avatar',
            'delete_avatar',
            )
        widgets = {'avatar': forms.FileInput()}

    def __init__(self, *args, **kwargs):
        """ Init styles """
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.attrs['autocomplete'] = 'off'
        self.helper.form_tag = False

    def clean_email(self):
        """ Check if email not already in use by other user """
        email = self.cleaned_data.get('email')
        if 'email' in self.changed_data:
            try:
                User.objects.get(email=email)
            except User.DoesNotExist:
                return email
            self.add_error('email', _("This email address is already used"))
        return email

    def clean_avatar(self):
        """
        Crop and resize avatar to 150px
        Validation could be made by validators
        but useful in clean before saving it
        """
        avatar = self.cleaned_data.get('avatar')
        if avatar and 'avatar' in self.changed_data:
            image = Image.open(avatar)
            # validate content type
            if image.format not in ['PNG', 'JPEG', 'GIF']:
                self.add_error('avatar', _("Image format must be JPEG, GIF ou PNG."))
            # validate file size
            if len(avatar) > (1000 * 1024):
                self.add_error('avatar', _("Image size must be lower than 1Mo."))
            avatar = resize_square(avatar, 'avatar')
        return avatar

    def save(self, commit=True):
        """
        Not necessary to check if username is not taken
        since it's done in the clean_email method
        """
        instance = super().save(commit=False)
        if commit:
            instance.save()
        return instance
  

#####################################################
#                   AVATARFORM                      #
#####################################################
class AvatarUpdateForm(forms.ModelForm):
    """
    Update an avatar via this form
    Uses an ajax request to /users/avatar
    """
    class Meta:
        model = User
        fields = ('avatar',)
        widgets = {
            'avatar': forms.FileInput(
                attrs={
                    'action': '/users/avatar',
                    'id': "form_avatar_file"}
            )}

