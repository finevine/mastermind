$(document).ready(function() {

    var clickSrcEl = null;
    var idTransfer = "";
    let reserve = $('.reserve .peg');
    let actives = $('.game .active');
    
    function selectpeg(e) {
      let current = parseInt($('#content').attr('data-current'));
      reserve.css('opacity', '1');
      $(this).css('opacity', '0.4');
      clickSrcEl = this;
      idTransfer = this.id;

      // refresh actives
      actives = $('.game .active');
      // if quick mode is not activated
      if (!$('body').hasClass("quick")) {
        // remove bounce to allow new bounce when changing
        actives.removeClass('bounce');
        // over effect on the peg
        actives.addClass('over');
      } else {
        var lastClass = actives[current].className.split(' ').pop();
        if (lastClass.startsWith("color")) {
          actives[current].classList.remove(lastClass);
        }
        // assign content of transfert to current peg
        actives[current].classList.add(idTransfer);
        idTransfer = "";
        // eq to convert DOM object to jquery object
        actives.eq(current).on('click', deletepeg);
        // active.length - 1 to take in consideration hints
        current = (current + 1) % (actives.length - 1);
        $('#content').attr('data-current', current);
        // if line full
        checklinecomplete();
      };
    }

    $('body').on('keypress',function(e) {
      if(e.which == 13 && $('.active .guess').attr('data-guess').length == 4) {
        $('.active .guess').click();
        $('#content').attr('data-current', '0');
      }
    });

    var droppeg = function(e) {
      if (e.stopPropagation) {
        e.stopPropagation(); // stops the browser from redirecting.
      }
      if (idTransfer) {
        peg = this
        // remove small cross to avoid new color to be deleted
        // $(this).off("click");
        // remove color
        this.classList.forEach(item=>{
          if(/^color/.test(item)) {
              peg.classList.remove(item) ;
          }
        })
        // make it bounce
        this.classList.add('bounce');
        // add newcolor
        this.classList.add(idTransfer);
        // // empty transfer
        idTransfer = "";
        // make reserve available (opacity 1)
        reserve.css('opacity', '1');
        // onclick : delete peg
        $(this).on('click', deletepeg)
        // remove over effect on the peg
        actives.removeClass('over');
        let complete = false;
        
        // if line full
        checklinecomplete();
      }
      return false;
    }

    var deletepeg = function() {
      peg = this
      this.classList.forEach(item=>{
        if(/^color/.test(item)) {
            peg.classList.remove(item) ;
        }
      })
      // this.removeEventListener("click", deletepeg);
      $('.active>.guess').attr('data-guess', '');
      $('.active>.guess').hide();
    }


    var checklinecomplete = function() {
      let line = $('.hints.active').attr('data-line');
      let combination = ''; 
      let pegs = $(".peg[data-line='"+line+"'][class*='color']");
      // if line full
      if (pegs.length == 4) {
        pegs.each(function(){
            number = $(this).attr('class').slice(-1);
            combination = combination + number;
        });
        $('.active>.guess').attr('data-guess', combination);
        $('.active>.guess').show();
      } else {
        $('.active>.guess').attr('data-guess', '');
        $('.active>.guess').hide();
      };
    }


    $('body').on('click', '.game .active', droppeg);
    $('body').on('click', '.reserve .peg', selectpeg);

  });
