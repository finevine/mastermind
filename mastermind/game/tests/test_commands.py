from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone
from datetime import datetime, timedelta
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from mastermind.game.models import Game
from tests.custom_test_case import CustomTestCase


class DeleteExpired(CustomTestCase):
    def test_command_permissions(self):
        self.creategame()
        self.game1.date_begin=timezone.now()-timedelta(hours=2)
        self.game1.save()
        self.assertEqual(Game.objects.count(), 2)
        call_command('delete_expired')
        self.assertEqual(Game.objects.count(), 1)
