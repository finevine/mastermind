import decimal
from django.test import TestCase
from mastermind.game.templatetags import gamefilters as filters

class CheckTags(TestCase):
    """ Test that tags return good results """

    def test_color(self):
        color = filters.color('1234', 1)
        self.assertEqual(color, '2')
        color = filters.color('', 1)
        self.assertEqual(color, '')