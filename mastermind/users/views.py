import os
from os import remove

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, get_user_model, login
from django.contrib.auth import views as auth_views
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.core.mail import EmailMessage
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.translation import LANGUAGE_SESSION_KEY, activate
from django.utils.translation import gettext as _
from django.views.decorators.http import require_POST
from django.views.i18n import translate_url

from mastermind.users.forms import (AvatarUpdateForm, LoginForm,
                                    UserCreationForm, UserUpdateForm)

User = get_user_model()

#####################################################
#                   REGISTRATION                    #
#####################################################
def login_view(request):
    """ Custom view for logging in using email """
    form = LoginForm(request.POST if request.method == 'POST' else None)
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, username=email, password=password)
        if user is not None:
            if user.last_login:
                request.session['last_login'] = user.last_login.timestamp()
            if user.is_active:
                language = user.language
                login(request, user)
                if request.POST.get('stay_connected', False):
                    request.session.set_expiry(600)
                else:
                    request.session.set_expiry(0)
                request.session[LANGUAGE_SESSION_KEY] = language
                next = request.GET.get('next')
                return redirect('game:new_game') if not next else redirect(next)
        else:
            messages.error(request, _("Invalid email or password"))
    title = _("Sign in")
    return render(request, 'users/registration/login.html', {'title': title, 'form': form, 'rules': True})


def signup_view(request):
    """ Custom view for creating a user """
    form = UserCreationForm(request.POST if request.method == 'POST' else None)
    if request.method == 'GET':
        return render(request, 'users/registration/signup.html', locals())

    if request.method == 'POST':
        if not form.is_valid():
            return render(request, 'users/registration/signup.html', locals())
        email = form.cleaned_data.get('email')
        # Create user
        user = form.save(commit=False)
        user.is_active = False
        user.save()
        # Send email for activation
        mail_subject = _("Confirm your email address")
        message = render_to_string(
            'users/registration/signup_validation_email.html',
            {
                'site': f'https://{get_current_site(request).domain}',
                'user': user,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': default_token_generator.make_token(user),
            })
        EmailMessage(mail_subject,message,from_email="contact@mastermind.ovh",to=[email]).send()
        return render(request, 'users/registration/signup_validation_begin.html')


def activate_user(request, uidb64, token):
    """
    Check token fit to user and decode uid
    Activate user if those are OK
    """
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        return render(request, 'users/registration/signup_validation_end.html')
    else:
        return render(request, 'users/registration/signup_validation_error.html')


class PasswordResetView(auth_views.PasswordResetView):
    template_name = "users/registration/password_reset_form.html"
    email_template_name = "users/registration/password_reset_email.html"
    subject_template_name = "users/registration/password_reset_subject.txt"
    success_url = reverse_lazy('users:password_reset_done')


class PasswordResetConfirmView(auth_views.PasswordResetConfirmView):
    template_name = "users/registration/password_reset_confirm.html"
    success_url = reverse_lazy('users:password_reset_complete')


class PasswordChangeView(auth_views.PasswordChangeView):
    template_name='users/user_password_change.html'


#####################################################
#                   USER UPDATE                     #
#####################################################
@login_required
def account(request):
    """ Render form to update datas of user and avatar """
    form = UserUpdateForm(instance=request.user)
    languages = settings.LANGUAGES
    title = _("Account")
    if request.method == 'POST':
        form = UserUpdateForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            user = form.save()
            avatar = form.cleaned_data.get('avatar', None)
            if avatar:
                request.user.avatar.save('avatar.jpeg', avatar, save=True)
            url = translate_url(reverse('users:account'), user.language)
            activate(user.language)
            if hasattr(request, 'session'):
                request.session[LANGUAGE_SESSION_KEY] = user.language
            if request.POST.get('delete_avatar'):
                user.avatar.delete()   #  <--- Efface le fichier
            messages.success(request, _("Information saved"))
            form = UserUpdateForm(instance=user)
            return redirect(url)
        return  render(request, 'users/user_account.html', locals())

    return render(request, 'users/user_account.html', locals())

@login_required
def delete_account(request):
    """ Permanently delete an account """
    if request.method == 'POST':
        request.user.delete()
        logout(request)
        return  redirect(reverse('users:index'))

    return render(request, 'users/user_delete.html')
