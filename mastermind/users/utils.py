from PIL import Image
from io import BytesIO
from django.core.files.base import ContentFile
from django.core.files.images import ImageFile


def crop_center(pil_img, crop_width, crop_height):
    """ Crop from the center of the image and return a PIL image

    args:
    pil_img -- PIL image
    crop_width -- with to crop pil_img
    crop_height -- height to crop pil_img
    """

    img_width, img_height = pil_img.size
    return pil_img.crop(((img_width - crop_width) // 2,
                        (img_height - crop_height) // 2,
                        (img_width + crop_width) // 2,
                        (img_height + crop_height) // 2))


def crop_max_square(pil_img):
    """ make a square from a rectangle PIL image """
    return crop_center(pil_img, min(pil_img.size), min(pil_img.size))


def resize_square(img, new_name, new_size=300):
    """
    Make a square image from a rectangle one
    and resize it to new_size with new_name
    """
    pil_img = Image.open(img).convert("RGBA")
    img = crop_max_square(pil_img).resize(
        (new_size, new_size), Image.LANCZOS
        )
    bg = Image.new("RGB", (new_size, new_size), (255,255,255))
    bg.paste(img, img)
    image_io = BytesIO()
    bg.save(fp=image_io, format="jpeg", quality=70)
    return ContentFile(image_io.getvalue())
    