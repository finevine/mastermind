import json

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from django.db.models import F
from django.db.models.aggregates import Count
from django.db.models.query import prefetch_related_objects
from django.http import HttpResponse
from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone
from django.utils.translation import gettext as _
from django.views.decorators.http import require_POST

from mastermind.users.decorators import has_perm_game

from .models import Game, Guess


@login_required
def new_game(request, go=None):
    ''' Create a game and start it if go == 'go' '''
    if go == 'go':
        game = Game.objects.create(player=request.user)
        return redirect(game)
    title = _('Ready?')
    return render(request, 'game/ready.html', locals())


@login_required
def game(request, game_id):
    ''' Return the game with remain_rounds so that player can resume a game '''
    game = get_object_or_404(Game, pk=game_id)
    try:
        last_guess = game.guesses.all()[0].num
    except IndexError:
        last_guess = 0
    remain_rounds = range(last_guess + 1, settings.ROUND_NUMBER + 1)
    if game.date_end and game.win:
        title = _('Win!')
    elif game.date_end:
        title = _('Try again!')
    else:
        title = _('Go!')
    return render(request, "game/go.html", locals())


@login_required
@require_POST
@has_perm_game
def guess(request, game_id):
    '''
    AJAX only
    if game belongs to player, return result of guess
    '''
    combination = request.POST['guess']
    game = get_object_or_404(Game, pk=game_id)
    if game.date_end:
        raise IntegrityError
    guess = Guess.objects.create(game=game, combination=combination)
    res = guess.result
    if guess.num == settings.ROUND_NUMBER:
        res['gameover'] = True
    return JsonResponse(res)


@login_required
@has_perm_game
def giveup(request, game_id):
    ''' Giveup current game '''
    game = get_object_or_404(Game, pk=game_id)
    if game.date_end:
        raise IntegrityError
    game.date_end = timezone.now()
    game.save()
    return redirect(game)


@login_required
@require_POST
@has_perm_game
def delete(request, game_id):
    '''
    AJAX Only
    if game belongs to player, player can delete it
    '''
    game = get_object_or_404(Game, pk=game_id)
    game.delete()
    return  HttpResponse(status=200)


@login_required
def ranking(request):
    ''' Display stats of games played by user and other players '''
    games = Game.objects.filter(
        date_end__isnull=False, win=True)\
        .prefetch_related('player', 'guesses') \
        .annotate(num_guesses=Count('guesses'), time=F('date_end') - F('date_begin')) \
        .order_by('num_guesses', 'time')[:100]
    title = _('Ranking')
    return render(request, "game/ranking.html", locals())


@login_required
def user_games(request):
    ''' Display stats of games played by user and other players '''
    games = Game.objects.filter(
        player=request.user)\
        .prefetch_related('player', 'guesses')
    title = _('My games')
    return render(request, "game/user_games.html", locals())