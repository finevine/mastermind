from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from mastermind.game.models import Game, Guess
from tests.custom_test_case import CustomTestCase

User = get_user_model()


class UserModelTest(CustomTestCase):
    def setUp(self) -> None:
        self.game = Game.objects.create(
            solution="1234",
            player=self.player1,
        )
        self.guess1 = Guess.objects.create(
            combination='4321',
            game=self.game
        )

    def test_game(self):
        self.assertEqual(self.game.duration, None)
        self.assertEqual(self.game.peg1, '1')
        self.assertEqual(self.game.peg2, '2')
        self.assertEqual(self.game.peg3, '3')
        self.assertEqual(self.game.peg4, '4')

    def test_guess_result(self):
        self.assertEqual(
            self.guess1.result,
            {
                'good_place': 0,
                'wrong_place': 4,
                'gameover': False}
        )
        self.assertEqual(self.game.win, False)
        guess2 = Guess.objects.create(
            combination='1234',
            game=self.game
        )
        self.assertEqual(
            guess2.result,
            {
                'good_place': 4,
                'wrong_place': 0,
                'gameover': False}
        )
        self.assertEqual(self.game.win, True)

    def test_guess_cheat(self):
        with self.assertRaises(ValidationError):
            for _ in range(settings.ROUND_NUMBER + 1):
                Guess.objects.create(
                    combination='1234',
                    game=self.game,
                )

    def test_guess_end(self):
        for _ in range(settings.ROUND_NUMBER - 1):
            Guess.objects.create(
                combination='1111',
                game=self.game,
            )
        self.assertIsNotNone(self.game.date_end)
        self.assertEqual(self.game.win, False)
 
    def test_guess_empty_guess(self):
        with self.assertRaises(ValidationError):
            Guess.objects.create(
                combination='124',
                game=self.game,
            )
