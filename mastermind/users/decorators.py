import functools

from django.core.exceptions import ObjectDoesNotExist, PermissionDenied

from mastermind.game.models import Game


def has_perm_game(func):
    """
    View decorator that check perm of a player on a game
    """
    @functools.wraps(func)
    def wrapper(request, *args, **kwargs):
        game_id = kwargs.get('game_id', None)
        try:
            game = Game.objects.get(pk=game_id)
            if request.user != game.player:
                raise PermissionDenied
        except ObjectDoesNotExist:
            pass
        return func(request, *args, **kwargs)

    return wrapper