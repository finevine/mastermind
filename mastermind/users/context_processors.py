from datetime import datetime
from django.utils.timezone import make_aware
from django.conf import settings
from django.urls import reverse, resolve, reverse_lazy
from django.shortcuts import get_object_or_404

def urlpath(request):
    return {"urlpath": request.path[3:]}


def version(request): # pragma: no cover
    version = open('version', 'r')
    return {'version': version.readlines()[0]}


def last_login(request):
    last_login = request.session.get('last_login')
    last_login_tz = make_aware(datetime.fromtimestamp(last_login)) if last_login else None
    return {'last_login': last_login_tz}
