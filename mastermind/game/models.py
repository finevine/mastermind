from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import get_supported_language_variant
from django.utils.translation import gettext as _

from .utils import solution_init

User = get_user_model()


class Game(models.Model):

    date_begin = models.DateTimeField(
        _("Begining of game"), auto_now_add=True)
    date_end = models.DateTimeField(
        _("End of game"), null=True)
    solution = models.CharField(
        _("Solution"), max_length=4, default=solution_init)
    win = models.BooleanField(
        "Win", default=False)
    player = models.ForeignKey(
        User, related_name="games", on_delete=models.CASCADE)

    class Meta:
        ordering = ['-date_end']
        default_permissions = ()
        permissions = (
            ('view_stats', _('Can play')),
        )

    @property
    def duration(self):
        if self.date_end:
            return  self.date_end - self.date_begin
        return None

    @property
    def peg1(self):
        return self.solution[0]

    @property
    def peg2(self):
        return self.solution[1]

    @property
    def peg3(self):
        return self.solution[2]

    @property
    def peg4(self):
        return self.solution[3]

    def get_absolute_url(self):
        """Get url for game detail view"""
        return reverse("game:detail", kwargs={"game_id": self.id})


class Guess(models.Model):
    date_played = models.DateTimeField(
        _("Played on the"), auto_now_add=True)
    combination = models.CharField(
        _("Combination"), max_length=4, blank=False)
    game = models.ForeignKey(
        _('Game'), related_name="guesses", on_delete=models.CASCADE, null=False)
    num = models.PositiveIntegerField(
        _("Number"), default=1
    )

    @property
    def color1(self):
        return self.combination[0]

    @property
    def color2(self):
        return self.combination[1]

    @property
    def color3(self):
        return self.combination[2]

    @property
    def color4(self):
        return self.combination[3]

    @property
    def result(self): 
        good_place, wrong_place = 0, 0
        solution = list(self.game.solution)
        combination = list(self.combination)
        # first list all good pegs
        # game.solution = '1141', result('1111') must be 3, 0 but not 3, 1
        for i in range(len(combination)):
            if combination[i] == solution[i]:
                good_place += 1
                # assign 2 different char to solution and combination
                solution[i] = '-'
                combination[i] = '%'
        for item in combination:
            if item in solution:
                wrong_place += 1
                to_remove = solution.index(item)
                solution[to_remove] = '-'
        return {'good_place': good_place, 'wrong_place': wrong_place, 'gameover': False}


    def save(self, *args, **kwargs):
        num_max_guess = 4
        try:
            last_guess = self.game.guesses.all()[0]
            self.num = last_guess.num + 1
        except IndexError:
            self.num = 1
        if self.num == settings.ROUND_NUMBER:
            self.game.date_end = timezone.now()
            self.game.save()
        elif self.num > settings.ROUND_NUMBER or self.game.win:
            raise ValidationError('La partie est finie')
        elif len(self.combination) != 4:
            raise ValidationError('Guess can not be empty')
        if self.combination == self.game.solution:
            self.game.date_end = timezone.now()
            self.game.win = True
            self.game.save()
                
        return super().save(*args, **kwargs)


    class Meta:
        ordering = ['-num']
        default_permissions = ()
