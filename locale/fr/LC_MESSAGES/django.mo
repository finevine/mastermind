��    `        �         (  L   )  x  v  �   �	  �   u
  5  0  �   f  u   F  M   �  Q   
     \  !   d  (   �  .   �     �     �     �     �               '     5     A     M     _     z     �     �     �     �     �     �     �     �     �     �  
   �     	          !     &     -  2   1     d     l  B   x  �   �  9   z  &   �  "   �     �          *  	   3     =     C     J  	   S     ]     `     g  	   w     �     �     �     �     �     �     �     �     �     �     �                  C         d     m     u     }     �     �     �     �     �  "   �     �  
   �     �     �     �     �  g   �     a     |  A  �  Q   �  q    �   �  �   $  y  �  �   o  �   d  p   �  W   W     �     �  -   �  3        7     G  
   [     f     n     �     �  
   �     �     �     �     �     	       	   !     +     @     \     b     r     y     �     �     �  	   �     �  
   �     �  .   �            A     �   X  ;   4   .   p   2   �      �   *   �      !     !     "!     /!     <!     H!     P!     T!     \!     t!  )   �!     �!     �!  	   �!     �!     �!     �!     �!  
   �!     �!     "     "     +"     J"  ,   V"     �"     �"  	   �"     �"     �"  
   �"     �"     �"     �"  $   �"     #     #     #     #     .#     2#  o   I#     �#     �#        0   _   5   C   E                 )       $   #       Y                 ^   2       H       A   Z   X      M   V   L       J      +   8             O   @      ?             K   ;   G   W   `       
       .   N   =          %           7   U   \   R      >             *               Q   I                  D   6       1          9         T   -   F          S       3                 B         ]       P   ,       4   	   [           /   !   &              :   '   <   "             (    
    All questions and answers remain pinned until the end of the game.
     
    At the beginning, the computer determines, a four-digit ordered color code chosen from six colors; each color can be used more than once. The player tries to figure out the code. To do this, he sets a similar color code as a question; on the first move he guesses blindly, on subsequent moves he guesses with the help of the answers to the previous moves, and so on.
     
    For colorblind people, an icon <i class="far fa-eye"></i> is available to add a pattern to pegs to distinguish them easily.
     
    It is sometimes easier to place pegs one by one beginning by a position your are sure of. Activate this option by clicking the finger icon <i class="far fa-hand-point-up"></i>.
     
    On each move the player gets the information <b>how many pegs he has set correctly in color and position</b> and <b>how many pegs have the right color but are in a wrong position</b>. A hit in color and position is indicated by a black pin, a correct colored peg in the wrong position by a white pin
     
    The game board is a perforated plate with rows of four round holes arranged one behind the other to hold color pieces and, parallel to each, four smaller holes in a square arrangement to hold black and white pins.
     
    To place pegs on the board, click the one you want to add on first place and then the second one and so on.
     
    To submit you guess <b>click the add button or press enter key</b>.
     
    Your goal is to guess the color code with as few questions as possible.
     Account Already have an account: sign in! An activation link has been sent by mail Are you sure you want to delete your account ? Avatar Begining of game Can play Cancel Change Password Check you emails Click to Play Colorblinds Combination Confirm password* Confirm your email address Create account Create an Account! Date Delete Delete account Delete my profile picture Email Email sent! Email* End of game English First Name Forgot Password? French Game Giveup Go! Goal: guess the code with as few guess as possible Guesses How to play I have red and accept the <a href="/terms">Terms</a> to subscribe. If a validated account exist with this email, you will receive an email for resetting your password. If not, please make sure you have subscribe with this account.and double check your spams If you don't find it double check you spams or be patient Image format must be JPEG, GIF ou PNG. Image size must be lower than 1Mo. Information saved Invalid email or password Language Last Name Login Logout My games Nickname* No Number Password change Password* Place pegs and submit guess Place pegs one by one Play Played on the Player Profile Pseudo Rank Ranking Ready to Leave? Ready? Remember me Reset password Save Select "Logout" below if you are ready to end your current session. Settings Sign In Sign Up Sign in Sign in! Sign up Solution Spanish Terms This email address is already used Time Try again! Win Win! Yes You may now You won't be able to access the site unless you recreate an account. <b>All your data will be lost</b>! Your account is now active sign in Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
Toutes les questions et les réponses restent affichées jusqu'à la fin du jeu. 
    Au début, l'ordinateur détermine un code à quatre couleurs choisies parmi six ; chaque couleur peut être utilisée plus d'une fois. Le joueur essaie de trouver le code. Pour ce faire, il procède par essaies/erreurs ; au premier coup, il devine à l'aveugle, aux coups suivants, il devine à l'aide des réponses des coups précédents, et ainsi de suite.
     
Pour les personnes daltoniennes, une icône <i class="far fa-eye"></i> est disponible pour ajouter un motif aux pions afin de les distinguer facilement. 
    Il est parfois plus facile de placer les pions un par un en commençant par une position dont vous êtes sûr. Activez cette option en cliquant sur l'icône du doigt <i class="far fa-hand-point-up"></i>. 
    À chaque déplacement, le joueur obtient les informations <b>Combien de pions il a posées correctement en couleur et en position</b> et <b>combien de pions ont la bonne couleur mais dans une mauvaise position</b>. Un bon placement en couleur et en position est indiqué par un petit pion noir, un pion de couleur correcte en mauvaise position par une un petit pion blanc 
    Le plateau de jeu est composé de 9 rangées de quatre trous ronds destinées à recevoir les jetons de couleur. A côté de chaque rangée quatre trous plus petits sont disposés en carré pour accueillir les réponses noires et blanches. 
    Pour placer des pions sur le plateau, cliquez d'abord sur celui que vous voulez ajouter puis le deuxième et ainsi de suite. 
    Pour valider votre essai, vous devez <b>cliquer sur le bouton d'ajout ou appuyer sur la touche entrée</b>. 
Votre objectif est de deviner le code couleur en faisant le moins d'essaies possibles. Compte Déjà un compte : Connexion ! Un lien d'activation a été envoyé par mail Êtes-vous sûr de vouloir supprimer votre compte ? Image de profil Début de la partie Peut jouer Annuler Changer de mot de passe Vérifiez vos emails Clique pour jouer Daltoniens Code Confirmez le mot de passe* Confirmez votre adresse email Créer un compte ! Créer un compte ! Date Supprimer Supprimer mon compte Effacer mon image de profil Email Email envoyé ! Email* Fin de la partie Anglais Prénom Mot de passe oublié ? Français Jeu Abandonner Partez ! But : deviner le code en un minimum de coups ! Coups Comment jouer J'ai lu et accepté les <a href="/terms">CGU</a> pour m'inscrire. Si un compte validé existe avec cet email, vous recevrez un email pour réinitialiser votre mot de passe. Si ce n'est pas le cas, veuillez vous assurer que vous vous êtes inscrit avec ce compte, et vérifiez vos spams Si vous ne l'avez pas, vérifiez vos spams patientez un peu L'image doit être au format JPEG, GIF oiu PNG La taille de l'image doit être inférieure à 1Mo Informations enregistées Nom d'utilisateur ou mot de passe invalide Langue Nom Se connecter Déconnexion Mes parties Pseudo* Non Numéro Changer de mot de passe Mot de passe* Placement des pions et validation du code Placer les pions un par un Jouer Jouée le Joueur Profil Pseudo Rang Classement Voulez-vous quitter ? Prêt ? Rester connecté Réinitialiser le mot de passe Enregistrer Vous êtes sur le point de vous déconnecter Paramètres Se connecter S'inscire Se connecter Se connecter ! S'inscrire Solution Espagnol CGU Cette adresse email est déjà prise Temps Perdu ! Gagné ! C'est gagné ! Oui Vous pouvez maintenant Vous ne pourrez plus accéder au site à moins de recréer un compte. <b>Toutes vos données seront perdues</b> Votre compte est activé Se connecter 