$(document).ready(function() {

    let table = $('#datatable').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "Tous"]
        ],
        "pageLength": 10,
        responsive: true,
        language: {
            processing:     gettext("Processing..."),
            search:         gettext("Filter:"),
            lengthMenu:     gettext("Show _MENU_ games"),
            info:           gettext("Showing _START_ to _END_ of _TOTAL_ games"),
            infoEmpty:      gettext("Showing 0 to 0 of 0 games"),
            infoFiltered:   gettext("(filtered from _MAX_ games)"),
            infoPostFix:    "",
            loadingRecords: gettext("Loading..."),
            zeroRecords:    gettext("No game to display"),
            emptyTable:     gettext("No game to display"),
            paginate: {
                first:      gettext("First"),
                previous:   "<",
                next:       ">",
                last:       gettext("Last")
            },
            aria: {
                sortAscending:  gettext(": activate to sort ascending order"),
                sortDescending: gettext(": activate to sort descending order")
            }
        },
        "autoWidth": true,
        "columns": [
            { className: "text-center" },
            { "width": "80px", className: "text-center" },
            null,
            { className: "text-center" },
            null,
            { className: "text-center" },
          ],

        "order": [[ 2, "desc" ]]
        //   "columnDefs": [
        //     { className: "text-center", "targets": [ 3 ] }
        //   ]

    });

    $('#datatable tbody').on( 'click', '.remove', function () {
        const csrftoken = Cookies.get('csrftoken')
        $tr = $(this).closest('tr');
        $row = $(this).parents('tr');
        url = $(this).data('url');
        if (confirm(gettext("Are you sure you want to delete this game?"))) {
            $.ajax({
                method: 'POST',
                headers: {'X-CSRFToken': csrftoken, 'X-Requested-With': 'XMLHttpRequest' },
                url: url,
                success: function(response){
                    table.row($tr).remove().draw();
                },
                error: function(response){
                }
            });
        };

    } );

});