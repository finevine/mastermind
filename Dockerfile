FROM python:3.8.3-alpine

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update && apk add postgresql-dev gcc python3-dev gettext
RUN apk add musl-dev freetype libpng libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev
RUN apk update && apk add openrc busybox-initscripts\
    && touch /var/log/cron.log

COPY . /app/
RUN cat ./mastermind/settings/delete_expired_crontab | crontab -

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

EXPOSE 8000

CMD ["gunicorn", "--bind", ":8000", "--workers", "3", "mastermind.wsgi:application", "--reload"]
