from mastermind.game.models import Game
import pytz
from django.contrib.auth.models import Permission
from django.test import TestCase

from mastermind.users.models import User


class CustomTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.player1 = User.objects.create(
            username="player1",
            first_name="Player",
            last_name="One",
            language='en',
            email="player1@player1.player1",
        )
        cls.player1.set_password('test')
        cls.player1.save()

        cls.player2 = User.objects.create(
            username="player2",
            first_name="Player",
            last_name="Two",
            language='en',
            email="player2@player2.player2",
            avatar="avatars/avatar.jpeg"
        )
        cls.player2.set_password('test')
        cls.player2.save()

        cls.player_inactive = User.objects.create(
            username="playerinactive",
            first_name="Player",
            last_name="Inactive",
            language='en',
            email="player_inactive@player_inactive.player_inactive",
            is_active=False,
        )
        cls.player_inactive.set_password('test')
        cls.player_inactive.save()

    def connectplayer1(self):
        self.client.force_login(self.player1)

    def connectplayer2(self):
        self.client.force_login(self.player2)

    def creategame(self):
        self.game1 = Game.objects.create(
            player=self.player1,
            solution="1234",
            )
        self.game2 = Game.objects.create(
            player=self.player1,
            solution="1234"
            )
