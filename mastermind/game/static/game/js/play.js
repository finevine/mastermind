$(document).ready(function() {
    let line_num = 1
    $("body").on('click', ".guess", function () {
        url = $(this).attr('data-url')
        data = {'guess': $(this).attr('data-guess')}
        const csrftoken = Cookies.get('csrftoken')
        line_num = $(this).attr('data-line')
        $.ajax({
            method: 'POST',
            url: url,
            headers: { 'X-CSRFToken': csrftoken, 'X-Requested-With': 'XMLHttpRequest' },
            data: data,
            success: function (response) {
                if (response['good_place'] == 4 || response['gameover']) {
                    document.location.reload()
                } else {
                    // hints current line fill with response
                    for (let i = 0; i < response['good_place']; i++) {
                        $(".hints.active").append('<span class="good_place"></span>')
                    }
                    for (let i = 0; i < response['wrong_place']; i++) {
                        $(".hints.active").append('<span class="wrong_place"></span>')
                    }
                    $(".active").off("click")
                    // active remove
                    $(".active").removeClass('active')
                    // reinitialize current
                    $('#content').attr('data-current', '0')
                    
                    // active add to new line
                    line_num = parseInt(line_num) + 1;
                    new_line = $(".peg[data-line='"+ line_num.toString() +"'],.hints[data-line='"+ line_num +"']")
                    new_line.addClass('active')
                }
            },
            error: function (response, status, error) {
                console.log('erreur')
            },
        });
      });

});
