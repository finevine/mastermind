document.addEventListener('DOMContentLoaded', (event) => {

    var clickSrcEl = null;
    var idTransfer = "";

    function selectToken(e) {

      reserve.forEach(function(item) {
        // change 3rd class by original id of token
        item.style.opacity = '1';
      });
      this.style.opacity = '0.4';
      clickSrcEl = this;
      idTransfer = this.id;
      tries.forEach(function (item) {
        // remove bounce to allow new bounce when changing
        item.classList.remove('bounce');
        // over effect on the token
        item.classList.add('over');
      });
    }


    function dropToken(e) {
      if (e.stopPropagation) {
        e.stopPropagation(); // stops the browser from redirecting.
      }
      if (idTransfer) {
        token = this
        // remove small cross to avoid new color to be deleted
        this.removeEventListener("click", deleteToken);
        // remove color
        this.classList.forEach(item=>{
          if(/^color/.test(item)) {
              token.classList.remove(item) ;
          }
        })
        // add newcolor
        this.classList.add(idTransfer);
        idTransfer = "";
        // make it bounce
        this.classList.add('bounce');

        // make reserve available (opacity 1)
        reserve.forEach(function(item) {
          // change 3rd class by original id of token
          item.style.opacity = '1';
        });
        // onclick : delete token
        this.addEventListener("click", deleteToken);

        tries.forEach(function (item) {
          // remove over effect on the token
          item.classList.remove('over');
        });
      }

      return false;
    }


    function deleteToken(e) {
      token = this
      this.classList.forEach(item=>{
        if(/^color/.test(item)) {
            token.classList.remove(item) ;
        }
      })
      this.removeEventListener("click", deleteToken);
    }


    function handleDragEnd(e) {
      this.style.opacity = '1';

      tries.forEach(function (item) {
        item.classList.remove('over');
      });
    }


    let reserve = document.querySelectorAll('.reserve .token');
    reserve.forEach(function(item) {
      item.addEventListener('click', selectToken, false);
    });

    let tries = document.querySelectorAll('.game .active');
    tries.forEach(function(item) {
      item.addEventListener('click', dropToken, false);
    });

  });
