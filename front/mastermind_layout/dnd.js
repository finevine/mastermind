document.addEventListener('DOMContentLoaded', (event) => {

    var dragSrcEl = null;

    function handleDragStart(e) {
      this.style.opacity = '0.4';

      dragSrcEl = this;

      e.dataTransfer.effectAllowed = 'move';
      e.dataTransfer.setData('text', this.id);
      // remove bounce to allow new bounce when changing
      tries.forEach(function (item) {
        item.classList.remove('bounce');
      });
    }

    function handleDragOver(e) {
      if (e.preventDefault) {
        e.preventDefault();
      }

      e.dataTransfer.dropEffect = 'move';

      return false;
    }

    function handleDragEnter(e) {
      // over effect on the token
      this.classList.add('over');
    }

    function handleDragLeave(e) {
      // over effect on the token
      this.classList.remove('over');
    }

    function deleteToken(e) {
      this.classList.remove(this.classList.item(2));
      this.classList.remove('chosen');
      this.removeEventListener("click", deleteToken);
    }

    function handleDrop(e) {
      if (e.stopPropagation) {
        e.stopPropagation(); // stops the browser from redirecting.
      }

      if (dragSrcEl != this) {
        // change 3rd class by original id of token
        this.classList.replace(
          this.classList.item(2),
          e.dataTransfer.getData('text')
          );
        // make it bounce
        this.classList.add('bounce');
        // onclick : delete token
        this.classList.add('chosen');
        this.addEventListener("click", deleteToken);
      }

      return false;
    }

    function handleDragEnd(e) {
      this.style.opacity = '1';

      tries.forEach(function (item) {
        item.classList.remove('over');
      });
    }


    let reserve = document.querySelectorAll('.reserve .token');
    reserve.forEach(function(item) {
      item.addEventListener('dragstart', handleDragStart, false);
      item.addEventListener('dragend', handleDragEnd, false);
    });

    let tries = document.querySelectorAll('.game .active');
    tries.forEach(function(item) {
      item.addEventListener('dragenter', handleDragEnter, false);
      item.addEventListener('dragover', handleDragOver, false);
      item.addEventListener('dragleave', handleDragLeave, false);
      item.addEventListener('dragend', handleDragEnd, false);
      item.addEventListener('drop', handleDrop, false);
    });

  });
