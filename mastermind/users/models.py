import uuid

import pytz
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


#####################################################
#                         USER                      #
#####################################################
class User(AbstractUser):
    """Default user"""
    username = models.CharField(
        _("Pseudo"), max_length=70, blank=False, unique=True, )
    first_name = models.CharField(
        _("First Name"), max_length=50)
    last_name = models.CharField(
        _("Last Name"), max_length=50)
    email = models.EmailField(
        _("Email"), unique=True, max_length=100)
    language = models.CharField(
        _("Language"),
        max_length=5, choices=settings.LANGUAGES,
        blank=True,
        default=settings.LANGUAGE_CODE)

    avatar = models.ImageField(
        _("Avatar"), upload_to='avatars',
        # validators=[image_validator],
        max_length=255, null=True, blank=True
        )

    class Meta:
        default_permissions = ()

    @property
    def full_name(self):
        return str(self.first_name + ' ' + self.last_name)

    def __repr__(self):
        return (
            f"User <Details => id: {self.id}"
            f" | email: {self.email} >"
        )

    def __str__(self):
        return self.full_name

    def get_absolute_url(self):
        """Get url for user's detail view.
        Returns:
        str: URL for user detail.
        """
        return reverse("users:account")
