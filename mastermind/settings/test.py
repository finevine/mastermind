import os
from .base import *

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'mastermind',
        'USER': 'postgres',
        'PASSWORD': '',
        'HOST': 'postgres',
        'PORT': '5432'

    }
}

ALLOWED_HOSTS = ['localhost', "0.0.0.0", "127.0.0.1"]


SECRET_KEY = os.environ.get('SECRET_KEY', 'g@po78@uzffdzfv=h8@cl70tk@2sG8f69/.!6bx85_w@s')

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = os.environ.get(
    "DJANGO_EMAIL_BACKEND", "django.core.mail.backends.console.EmailBackend"
)