from django.apps import AppConfig


class UsersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mastermind.users'

    def ready(self):
        import mastermind.users.signals  # noqa
