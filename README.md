# Mastermind

Mastermind is a multilingual online mastermind. The solution of the game is computed on the server side wich prevent geeks to cheat.
It also offers a worlwide ranking of all players.

## Requirements

To install run mastermind locally, you will need:

- Python3 installed
- Git installed
- Docker desktop or Docker engine installed

### Clone repository

```bash
git clone https://gitlab.com/finevine/mastermind.git .
```

### Run the dockers

```bash
docker-compose -f docker-compose.local.yml up -d
```

### Run the migrations and install the first user

```bash
docker-compose -f docker-compose.local.yml exec web python3 manage.py migrate
docker-compose -f docker-compose.local.yml exec web python3 manage.py devsuperuser --username <yourusername> --email <your@email.com> --password <yourpassword>
```
Changing values in `< >`

### Access the application

The application is now available in your web browser at **http://0.0.0.0:8000**

## Troubleshooting

If you meet any issue deploying the application, feel free to open an issue.
