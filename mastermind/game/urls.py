from django.urls import path
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from mastermind.game import views

app_name = 'game'

urlpatterns = [
    path("", TemplateView.as_view(template_name='game/index.html'), name="index"),
    path("<int:game_id>/", views.game, name="detail"),
    path("<int:game_id>/giveup/", views.giveup, name="giveup"),
    path("new/", views.new_game, name="new_game"),
    path("ranking/", views.ranking, name="ranking"),
    path("my-games/", views.user_games, name="user_games"),
    path("new/<str:go>/", views.new_game, name="new_game"),
    path("<int:game_id>/guess/", views.guess, name="guess"),
    path("<int:game_id>/delete/", views.delete, name="delete"),
]
