from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):
    help = 'Create a superuser'

    def add_arguments(self, parser):
        parser.add_argument('--username', type=str, help='username for superuser')
        parser.add_argument('--email', type=str, help='Email for superuser')
        parser.add_argument('--password', type=str, help='Password for superuser')

    def handle(self, *args, **kwargs):
        """create superuser """
        self.stdout.write("Creating superuser")
        admin_user = User.objects.create(
            email=kwargs['email'],
            is_superuser=True,
            is_active=True
        )
        admin_user.set_password(kwargs['password'])
        admin_user.save()
