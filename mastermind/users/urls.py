from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.urls import path
from django.views.generic import TemplateView

from mastermind.users import views

app_name = 'users'

urlpatterns = [
    path("", views.login_view, name="index"),
    path('signup/', views.signup_view, name='signup'),
    path('account/', views.account, name='account'),
    path('delete-account/', views.delete_account, name='delete_account'),
    path("signup/<uuid:token>/", views.signup_view, name="signup"),
    path('activate/<uidb64>/<token>/', views.activate_user, name='activate'),
    # django.contrib.auth views
    path('signin/', views.login_view, name='signin'),
    path('password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name="users/registration/password_reset_done.html"), name='password_reset_done'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name="users/registration/password_reset_complete.html"), name='password_reset_complete'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
