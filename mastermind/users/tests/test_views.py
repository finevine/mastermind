import pathlib
import tempfile
from django.contrib import auth
from django.contrib.auth.tokens import default_token_generator
from django.test import override_settings
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from tests.custom_test_case import CustomTestCase
from PIL import Image


User = auth.get_user_model()


class TestRegistration(CustomTestCase):

    def test_activate_0(self):
        """ Test activation of player inactive """
        uid = urlsafe_base64_encode(force_bytes(self.player_inactive.pk))
        token = default_token_generator.make_token(self.player_inactive) + 'invalid'
        kwargs = {'uidb64': uid, 'token': token}
        self.assertEqual(self.player_inactive.is_active, False)
        response = self.client.get(
            reverse('users:activate', kwargs=kwargs))
        self.assertTemplateUsed(
            response,
            'users/registration/signup_validation_error.html')
        self.player_inactive.refresh_from_db()
        self.assertEqual(self.player_inactive.is_active, False)


    def test_activate_1(self):
        """ Test activation of player inactive """
        uid = urlsafe_base64_encode(force_bytes(self.player_inactive.pk))
        token = default_token_generator.make_token(self.player_inactive)
        kwargs = {'uidb64': uid, 'token': token}
        self.assertEqual(self.player_inactive.is_active, False)
        response = self.client.get(
            reverse('users:activate', kwargs=kwargs))
        self.assertTemplateUsed(
            response,
            'users/registration/signup_validation_end.html')
        self.player_inactive.refresh_from_db()
        self.assertEqual(self.player_inactive.is_active, True)


    def test_login(self):
        """ Test login of a user """
        # TEST GET METHOD
        response_get = self.client.get(reverse('users:signin'))
        self.assertTemplateUsed(response_get, 'users/registration/login.html')
        # TEST NON EXISTING USER
        data = {'email': 'nonexisting@user.com', 'password': 'secret12'}
        response = self.client.post(reverse('users:signin'), data)
        self.assertEqual(response.status_code, 200)
        user = auth.get_user(self.client)
        self.assertEqual(user.is_authenticated, False)
        # TEST EXISTING USER
        data = {'email': 'player1@player1.player1', 'password': 'test'}
        self.client.post(reverse('users:signin'), data)
        user = auth.get_user(self.client)
        self.assertEqual(user.is_authenticated, True)

    def test_signup_post_good_data(self):
        """ Test if user is well created """
        user_count = User.objects.count()
        form_data = {
            'username': 'player',
            'email': 'player@player.player',
            'password1': '5f99c501b46a',
            'password2': '5f99c501b46a',
            'cgu_check': True}
        response = self.client.post(
            reverse('users:signup'),
            form_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), user_count+1)
        user4 = User.objects.get(email='player@player.player')
        # assert account not validated yet
        self.assertEqual(user4.is_active, False)

    def test_signup_post_bad_data(self):
        """ Test if already taken email raise error """
        user_count = User.objects.count()
        form_data = {
            'email': 'player1@player1.player1',
            'password1': '5f99c501b46a',
            'password2': '5f99c501b46a',
            'cgu_check': True}
        response = self.client.post(
            reverse('users:signup'),
            form_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), user_count)
        self.assertContains(
            response, 'This email address is already used', )

    def test_signup_get(self):
        """ Test if user is good created """
        response = self.client.get(reverse('users:signup'))
        self.assertEqual(response.status_code, 200)
        # Test if response contains CGU field begining with 'I have red'
        self.assertContains(response, 'I have red and accept')


#####################################################
#                PROFILE UPDATE                     #
#####################################################
class TestProfilePageUpdate(CustomTestCase):

    def test_user_update(self):
        """ Test if a user is updated correctly """
        self.connectplayer1()
        # UPDATE EMAIL WITH EXISTING ONE
        data = {
            'email': 'player2@player2.player2',
            'last_name': 'Doe', 'first_name': 'John',
        }
        response = self.client.post(reverse('users:account'), data=data)
        self.assertContains(response, "This email address is already used")

        # UPDATE EMAIL
        data = {
            'username': 'playerA',
            'email': 'playerA@playerA.playerA',
            'last_name': 'Doe', 'first_name': 'John',
        }
        response = self.client.post(reverse('users:account'), data=data, follow=True)
        self.assertTemplateUsed('users/account.html')
        self.assertContains(response, "Information saved")


#####################################################
#                       AVATAR                      #
#####################################################
MEDIA_ROOT = tempfile.mkdtemp()


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class TestAvatar(CustomTestCase):
    """ Test an avatar update, a deletion after update """

    def _create_image(self, format):
        """ Create temp images with tempfile for deletion in teardown """
        with tempfile.NamedTemporaryFile(suffix='.' + format, delete=False) as f:
            image = Image.new('RGB', (200, 200), 'white')
            image.save(f, format)
        return open(f.name, mode='rb')

    def setUp(self):
        self.avatar0 = self._create_image('jpeg')
        self.avatar1 = self._create_image('jpeg')
        self.avatar2 = self._create_image('jpeg')

    def tearDown(self):
        self.avatar0.close()
        self.avatar1.close()
        self.avatar2.close()

    def test_avatar_update(self):
        """
        Check if avatar have good name and is well deleted on change
        A first user has to connect first to create avatar.jpg
        """
        # connect a user to create an "avatar.jpg" file
        self.connectplayer2()
        data = {
            'username': 'player2',
            'email': 'player2@player2.player2',
            'avatar': self.avatar0,
        }
        self.client.post(reverse('users:account'), data=data)

        self.connectplayer1()
        data = {
            'username': 'player1',
            'email': 'player1@player1.player1',
            'avatar': self.avatar1,
        }
        response = self.client.post(reverse('users:account'), data=data)
        player1 = User.objects.get(email='player1@player1.player1')
        path = player1.avatar.path

        # change avatar
        data = {
            'username': 'player1',
            'email': 'player1@player1.player1',
            'avatar': self.avatar2,
        }
        self.client.post(reverse('users:account'), data=data)

        player1 = User.objects.get(email='player1@player1.player1')
        new_path = player1.avatar.path
        # gif converted to jpeg
        self.assertEqual("jpeg", path.split(".")[-1])
        # path should be destroyed
        self.assertEqual(pathlib.Path(path).exists(), False)
        # new_path should be OK
        self.assertEqual(pathlib.Path(new_path).exists(), True)

