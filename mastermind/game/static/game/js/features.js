function disableColorBlind() {
  document.styleSheets[1].disabled = true;
}

function toggleColorBlind(e){
  var stylesheet = document.styleSheets[1];
  stylesheet.disabled = ! stylesheet.disabled;
  var eyes = document.querySelectorAll('.options > i');
  $(".colorblind-button").toggle();
}

function toggleQuickSelect(e){
  $('body').toggleClass('quick');
  $(".quick-button").toggle();
  $(".active").removeClass('over');
  $('#content').attr('data-current', '0');
}
