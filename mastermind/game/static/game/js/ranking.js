$(document).ready(function() {

    $('#datatable').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "Tous"]
        ],
        "pageLength": 10,
        responsive: true,
        language: {
            processing:     gettext("Processing..."),
            search:         gettext("Filter:"),
            lengthMenu:     gettext("Show _MENU_ games"),
            info:           gettext("Showing _START_ to _END_ of _TOTAL_ games"),
            infoEmpty:      gettext("Showing 0 to 0 of 0 games"),
            infoFiltered:   gettext("(filtered from _MAX_ games)"),
            infoPostFix:    "",
            loadingRecords: gettext("Loading..."),
            zeroRecords:    gettext("No game to display"),
            emptyTable:     gettext("No game to display"),
            paginate: {
                first:      gettext("First"),
                previous:   "<",
                next:       ">",
                last:       gettext("Last")
            },
            aria: {
                sortAscending:  gettext(": activate to sort ascending order"),
                sortDescending: gettext(": activate to sort descending order")
            }
        },
        "autoWidth": true,
        "columns": [
            null,
            null,
            { "width": "80px", classname: "text-center" },
            null,
            null,
            null,
          ],
        "ordering": false,
        "order": [[ 3, "asc" ], [ 4, "asc"]]
        //   "columnDefs": [
        //     { className: "text-center", "targets": [ 3 ] }
        //   ]

    });

});