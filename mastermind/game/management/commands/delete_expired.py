
import pytz
from django.utils import timezone
from datetime import timedelta, datetime
from django.core.management.base import BaseCommand, CommandError

from mastermind.game.models import Game


class Command(BaseCommand):
    help = 'Delete expired games'

    def handle(self, *args, **kwargs):
        delta = timedelta(hours=1)
        expire_date = timezone.now().replace(tzinfo=pytz.utc) - delta
        games = Game.objects.filter(
            date_end__isnull=True,
            date_begin__lt=expire_date
        )
        self.stdout.write(f'Deleting {games.count()} expired games')
        games.delete()
        self.stdout.write("Deletion completed")