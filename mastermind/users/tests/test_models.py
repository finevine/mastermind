from django.test import TestCase
from django.contrib.auth import get_user_model
from tests.custom_test_case import CustomTestCase

User = get_user_model()


class UserModelTest(CustomTestCase):

    def test_full_name(self):
        self.assertEqual(self.player1.full_name, 'Player One')

    def test_absolute_url(self):
        self.assertEqual(self.player1.get_absolute_url(), f'/en/account/')
