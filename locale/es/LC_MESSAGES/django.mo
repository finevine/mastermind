��    `        �         (  L   )  x  v  �   �	  �   u
  5  0  �   f  u   F  M   �  Q   
     \  !   d  (   �  .   �     �     �     �     �               '     5     A     M     _     z     �     �     �     �     �     �     �     �     �     �  
   �     	          !     &     -  2   1     d     l  B   x  �   �  9   z  &   �  "   �     �          *  	   3     =     C     J  	   S     ]     `     g  	   w     �     �     �     �     �     �     �     �     �     �     �                  C         d     m     u     }     �     �     �     �     �  "   �     �  
   �     �     �     �     �  g   �     a     |  A  �  Q   �  �    �   �  �   9  �      �  x   �  `     W   u     �     �  8   �  (   )     R     d     z     �     �     �     �  
   �     �     �  ,   �     "     0     ?     E     S     a     |     �     �     �     �     �     �     �     �  	   �     �  6        9  
   ?  D   J  �   �  <   u   2   �   5   �       !  '   <!     d!  	   k!  
   u!     �!     �!     �!     �!     �!     �!     �!  7   �!     
"     %"  	   +"     5"     ="  
   E"     P"     _"     n"  	   {"     �"     �"     �"     �"  
   �"  
   �"     �"  
   �"     #     #  	    #     *#     3#  7   8#     p#     w#  	   �#  	   �#     �#     �#  Y   �#     $  
   $        0   _   5   C   E                 )       $   #       Y                 ^   2       H       A   Z   X      M   V   L       J      +   8             O   @      ?             K   ;   G   W   `       
       .   N   =          %           7   U   \   R      >             *               Q   I                  D   6       1          9         T   -   F          S       3                 B         ]       P   ,       4   	   [           /   !   &              :   '   <   "             (    
    All questions and answers remain pinned until the end of the game.
     
    At the beginning, the computer determines, a four-digit ordered color code chosen from six colors; each color can be used more than once. The player tries to figure out the code. To do this, he sets a similar color code as a question; on the first move he guesses blindly, on subsequent moves he guesses with the help of the answers to the previous moves, and so on.
     
    For colorblind people, an icon <i class="far fa-eye"></i> is available to add a pattern to pegs to distinguish them easily.
     
    It is sometimes easier to place pegs one by one beginning by a position your are sure of. Activate this option by clicking the finger icon <i class="far fa-hand-point-up"></i>.
     
    On each move the player gets the information <b>how many pegs he has set correctly in color and position</b> and <b>how many pegs have the right color but are in a wrong position</b>. A hit in color and position is indicated by a black pin, a correct colored peg in the wrong position by a white pin
     
    The game board is a perforated plate with rows of four round holes arranged one behind the other to hold color pieces and, parallel to each, four smaller holes in a square arrangement to hold black and white pins.
     
    To place pegs on the board, click the one you want to add on first place and then the second one and so on.
     
    To submit you guess <b>click the add button or press enter key</b>.
     
    Your goal is to guess the color code with as few questions as possible.
     Account Already have an account: sign in! An activation link has been sent by mail Are you sure you want to delete your account ? Avatar Begining of game Can play Cancel Change Password Check you emails Click to Play Colorblinds Combination Confirm password* Confirm your email address Create account Create an Account! Date Delete Delete account Delete my profile picture Email Email sent! Email* End of game English First Name Forgot Password? French Game Giveup Go! Goal: guess the code with as few guess as possible Guesses How to play I have red and accept the <a href="/terms">Terms</a> to subscribe. If a validated account exist with this email, you will receive an email for resetting your password. If not, please make sure you have subscribe with this account.and double check your spams If you don't find it double check you spams or be patient Image format must be JPEG, GIF ou PNG. Image size must be lower than 1Mo. Information saved Invalid email or password Language Last Name Login Logout My games Nickname* No Number Password change Password* Place pegs and submit guess Place pegs one by one Play Played on the Player Profile Pseudo Rank Ranking Ready to Leave? Ready? Remember me Reset password Save Select "Logout" below if you are ready to end your current session. Settings Sign In Sign Up Sign in Sign in! Sign up Solution Spanish Terms This email address is already used Time Try again! Win Win! Yes You may now You won't be able to access the site unless you recreate an account. <b>All your data will be lost</b>! Your account is now active sign in Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
Todas las preguntas y respuestas permanecen publicadas hasta el final del juego. 
Al principio, el ordenador determina un código de cuatro colores , elegido entre seis; cada color puede utilizarse más de una vez. El El jugador intenta encontrar el código. Para ello, procede por ensayo y error; en la primera jugada, adivina a ciegas, en las siguientes, adivina con la ayuda del adivina usando las respuestas de las jugadas anteriores, y así sucesivamente.
     
Para los daltónicos, existe un icono <i class="far fa-eye"></i> añadir un patrón <i class="far fa-eye"></i> a los contadores para distinguirlos fácilmente 
A veces es más fácil colocar las piezas de una en una empezando por una posición de la que estás seguro. Active esta opción haciendo clic en el icono del dedo <i class="far fa-hand-point-up"></i>. 
Con cada movimiento, el jugador obtiene la información de <b>cuántas fichas ha colocado</b>.correctamente en color y posición</b> y <b>cuántas fichas tienen la correcta color pero en la posición incorrecta</b>. Una colocación correcta en color y posición es indicado por un pequeño peón negro, un peón de color correcto en la posición incorrecta por un un pequeño peón blanco 
 El tablero de juego es una placa perforada con filas de cuatro agujeros redondos dispuestas una detrás de otra para acomodar las piezas de color y, paralelas a cada una,cuatro agujeros más pequeños dispuestos en un cuadrado para acomodar las clavijas blancas y negras 
Para colocar las piezas en el tablero, primero haz clic en la que quieras añadir luego el segundo y así sucesivamente 
Para validar su redacción, debe <b>hacer clic en el botón de añadir o pulsar enter</b>.
     
Su objetivo es adivinar el código de colores en el menor número de intentos posible. Cuenta Ya una cuenta : conexión ! Un enlace de activación le fue mandado mandado por mail esta seguro que quiere borrar su cuenta? Imagen de perfíl Principio del partido Puede jugar Annular Cambiar contraseña Comprueba sus mails hacer clic para jugar Daltonicos Codigo Confirmar contraseña* Confirmar su direccion de correo electronico Crear Cuenta! Crear cuenta ! Fecha Borrar cuenta Borrar cuenta Eliminar mi foto de perfil Email Email mandado! Correo electronico* Partido terminado Inglés Nombre ¿Contraseña olivdada? Francés Partido Abandonar ¡Vamos! objetivo : adivinar el codigo con un minimo de turnos! Turno Como jugar He leido y acceptado las <a href="/terms">T&Cs</a> para inscribirme. Si existe una cuenta validada con este correo electrónico, recibirá un correo electrónico a  para restablecer su contraseña. Si no es el caso, por favor asegúrate de que te has registrado con esta cuenta, y comprueba tu spam Si no lo tiene, comprueba en tus spams o espera un momentito La imagen tiene que ser al formato JPEG, GIF o PNG El tamaño de la imagen tiene que ser de menos de 1Mo Informaciones gravadas/guardadas Nombe de usuario o contraseña invalida Idioma Appellido Conectarse Desconexión Mis partidos seudónimo* No Nùmero Cambiar contraseña Contraseña* Colocación de los contadores y validación del código Placer les pions un par un Jugar Jugado el Jugador Perfíl seudónimo Clasificación Clasificación quiere irse? ¿Listo ? Quedarse conectado Reinicialisar contraseña Registrarse Esta a punto de desconctarse Parametros Conectarse Inscribirse Conectarse Conectarse! Inscribirse Solución Español T&Cs Esta dirección de correo electrónico ya está ocupada Tiempo ¡Intenta otra vez! ¡Ganado! ¡Ganado! Sí Ya puede No podrás mas acceder al sitio sin crear nueva cuenta. <b>Todos datos seran perdidos</b> Su cuenta es activada conectarse 