��          �      �       0     1  "   M  #   p  *   �     �     �     �     �     �     �       )     A  I  "   �  3   �  6   �  3     	   M     W     `     i     �     �  !   �  1   �         
   	                                              (filtered from _MAX_ games) : activate to sort ascending order : activate to sort descending order Are you sure you want to delete this game? Filter: First Last No game to display Processing... Show _MENU_ games Showing 0 to 0 of 0 games Showing _START_ to _END_ of _TOTAL_ games Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 (filtré de _MAX_ parties au total : activer pour trier la colonne par ordre croissant : activer pour trier la colonne par ordre décroissant Êtes-vous sûr de vouloir supprimer cette partie ? Filtrer : Premiers Derniers Aucune partie à afficher Traitement en cours... Afficher _MENU_ parties Affichage de 0 à 0 parties sur 0 Affichage de _START_ à _END_ parties sur _TOTAL_ 