function disableColorBlind() {
  document.styleSheets[1].disabled = true;
}

function toggleColorBlind(e){
  var stylesheet = document.styleSheets[-1];
  stylesheet.disabled = ! stylesheet.disabled;
  var eyes = document.querySelectorAll('.options > i');
  eyes.forEach(function(item){
    item.classList.toggle('hide');
  });
}
