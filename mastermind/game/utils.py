import random

def solution_init():
    ''' Random selection random choice '''
    return ''.join(str(k) for k in random.choices(range(6), k=4))